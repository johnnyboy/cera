# README #

### CERA - CurrencyExchangeRateAggregator ###

 CERA is a PoC for the following technology stack

+ [Apache Kafka](https://kafka.apache.org/)
+ [Spring Boot](https://projects.spring.io/spring-boot/)
+ [Bootstrap](http://getbootstrap.com/)
+ [Mustache](https://mustache.github.io/)

### Brief system overview ###

Several things happen at the application startup

+ Kafka topics are verified and optionally created
+ Kafka producer queries several free exchange rate web-services and puts the results into respective topics
+ Kafka consumer polls the topics and stores the data into an in-memory storage which is used by the GUI

CERA is capable of operating in three modes

+ Real time single web service - select a service and request one of the supported currency pairs rate
+ Real time multiple web services - so called 'aggregated' mode - much like the previous instead the dedicated rate is fetched from all web-services at once
+ Kafka-based mode - data comes from Kafka topics, for each and every batch of rates Min, Max and Avg values are calculated

### How do I get set up? ###

#### Kafka setup ####

[Official kafka quickstart](https://kafka.apache.org/quickstart)

Time for a few tweaks at `$KAFKA_INSTALL_DIR/config/server.properties`

##### Server Basics #####
+ auto.create.topics.enable=true
+ delete.topic.enable=true

##### Log retention policy #####
+ log.retention.hours=1

Start `zookeeper-server` and `kafka-server`

#### Clone the repository, import the maven-based project into your favorite IDE ####
+ [Eclipse-based Spring Tool Suite](https://spring.io/tools/sts)
+ [IntelliJ IDEA](https://www.jetbrains.com/idea/)

#### Run the project as a Spring Boot application ####
Navigate to [http://localhost:8080/cera](http://localhost:8080/cera)

![cera-home.png](https://bitbucket.org/repo/koLo9z/images/843086851-cera-home.png)