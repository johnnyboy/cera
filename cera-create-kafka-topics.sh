#!/bin/bash
KAFKA_BIN_PATH="/home/tbyndyu/kafka/kafka_2.11-0.10.1.0/bin"

echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic PLN_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic PLN_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic PLN_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic PLN_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic PLN_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic UAH_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic UAH_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic UAH_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic UAH_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic UAH_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic USD_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic USD_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic USD_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic USD_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic USD_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic GBP_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic GBP_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic GBP_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic GBP_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic GBP_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic EUR_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic EUR_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic EUR_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic EUR_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic EUR_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic CNY_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic CNY_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic CNY_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic CNY_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --partitions 1 --replication-factor 1 --topic CNY_EUR`