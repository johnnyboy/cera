package com.taurus.cera.service.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.base.Optional;
import com.taurus.cera.configuration.ExchangeRateServiceConfiguration;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.CurrencyPairs;
import com.taurus.cera.model.ExchangeRate;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class ExchangeRateServiceIntegrationTest {

	@Autowired
	private Collection<ExchangeRateService> services;

	@TestConfiguration
	@Import(value = { ExchangeRateServiceConfiguration.class })
	@ComponentScan(basePackages = { "com.taurus.cera.service.web" })
	static class ServiceTestConfiguration {
	}

	@Test
	public void servicesAreAutowired() {
		assertThat(services).isNotEmpty();
	}

	@Test
	public void allServicesRespondToAllCurrencyPairs() {
		for (ExchangeRateService service : services) {
			for (CurrencyPair pair : CurrencyPairs.all()) {
				Optional<ExchangeRate> optionalRate = service.getRate(pair);
				assertThat(optionalRate).isNotNull();

				if (!optionalRate.isPresent()) {
					continue;
				}

				ExchangeRate rate = optionalRate.get();
				assertThat(rate).isNotNull();
				assertThat(rate.getCurrencyPair()).isEqualTo(pair);
				assertThat(rate.getValue()).isNotNull();
				assertThat(rate.getSource()).isNotNull();
				assertThat(rate.getTimestamp()).isNotNull();
				assertThat(rate.getTitle()).isNotEmpty();
			}
		}
	}
}
