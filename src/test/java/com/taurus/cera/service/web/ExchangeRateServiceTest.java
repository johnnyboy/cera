package com.taurus.cera.service.web;

import static com.taurus.cera.model.CurrencyCode.GBP;
import static com.taurus.cera.model.CurrencyCode.PLN;
import static com.taurus.cera.model.CurrencyCode.UAH;
import static com.taurus.cera.model.CurrencyCode.USD;
import static com.taurus.cera.model.CurrencyPair.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceTest {

	@InjectMocks
	private AbstractExchangeRateService objectUnderTest = new YahooExchangeRateService();

	@Mock
	private ObjectMapper mapperMock;

	@Mock
	private RestTemplate templateMock;

	@Mock
	private URLResolver urlResolverMock;

	@Test
	public void objectMapperRestTemplateAndURLResolverAreMockedAndInjected() {
		assertThat(objectUnderTest.objectMapper).isEqualTo(mapperMock);
		assertThat(objectUnderTest.restTemplate).isEqualTo(templateMock);
		assertThat(objectUnderTest.urlResolver).isEqualTo(urlResolverMock);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void emptyOptionalIsReturnedUponRestClientExceptionOrIOException() {
		// given
		given(urlResolverMock.resolve(any(String.class), any(CurrencyPair.class))).willReturn("URL");
		given(templateMock.getForObject(any(String.class), isA(Class.class)))
				.willThrow(RestClientException.class, IOException.class);

		// when RestClientException
		Optional<ExchangeRate> rate = objectUnderTest
				.getRate(of(USD, GBP));

		// then
		assertThat(rate.isPresent()).isFalse();

		// when IOException
		rate = objectUnderTest
				.getRate(of(UAH, PLN));

		// then
		assertThat(rate.isPresent()).isFalse();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void isAliveReturnsFalseUponRestClientException() {
		// given
		given(urlResolverMock.resolve(any(String.class), any(CurrencyPair.class))).willReturn("URL");
		given(templateMock.exchange(any(String.class), any(HttpMethod.class), isNull(HttpEntity.class),
				isA(Class.class)))
						.willThrow(RestClientException.class);

		// when
		boolean isAlive = objectUnderTest.isAlive();

		// then
		assertThat(isAlive).isFalse();
	}

	@Test
	@SuppressWarnings("unchecked")
	public void isAliveReturnsTrueOnAnyResponseWithBody() {
		// given
		ResponseEntity<?> responseMock = mock(ResponseEntity.class);
		given(responseMock.hasBody()).willReturn(true);
		given(urlResolverMock.resolve(any(String.class), any(CurrencyPair.class))).willReturn("URL");
		given(templateMock.exchange(any(String.class), any(HttpMethod.class), isNull(HttpEntity.class),
				isA(Class.class))).willReturn(responseMock);

		// when
		boolean isAlive = objectUnderTest.isAlive();

		// then
		assertThat(isAlive).isTrue();
	}

}
