package com.taurus.cera.service.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;

public class URLResolverTest {

	@Test
	public void fromAndToPatternsAreReplacedWithCurrencyPairCodes() {
		// given
		String fromPattern = "from";
		String toPattern = "to";
		String baseURL = "http://whatever.rate-exchange.com/from-to";
		CurrencyPair pair = CurrencyPair.of(CurrencyCode.EUR, CurrencyCode.USD);

		// when
		String resolvedURL = new URLResolver(fromPattern, toPattern).resolve(baseURL, pair);

		// then
		assertThat(resolvedURL).containsIgnoringCase(pair.getFrom().name());
		assertThat(resolvedURL).containsIgnoringCase(pair.getTo().name());
		assertThat(resolvedURL).doesNotContain(fromPattern);
		assertThat(resolvedURL).doesNotContain(toPattern);
	}
}
