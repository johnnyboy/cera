package com.taurus.cera.model;

import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.fill;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class StatisticalExchangeRateTest {

	@Mock
	private CurrencyPair pairMock;

	@Mock
	private Collection<KafkaExchangeRate> kafkaRatesMock;

	@InjectMocks
	private StatisticalExchangeRate objectUnderTest;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void averageMinMaxReturnDefaultValueIfNoRatesGiven() {
		// given
		given(kafkaRatesMock.isEmpty()).willReturn(true);
		BigDecimal defaultValue = BigDecimal.ZERO;

		// when
		BigDecimal minimalRate = objectUnderTest.getMinimalRate();
		BigDecimal maximumRate = objectUnderTest.getMaximumRate();
		BigDecimal averageRate = objectUnderTest.getAverageRate();

		// then
		assertThat(minimalRate).isEqualTo(defaultValue);
		assertThat(maximumRate).isEqualTo(defaultValue);
		assertThat(averageRate).isEqualTo(defaultValue);
	}

	@Test
	@Parameters(method = "minimalVerificationParameters")
	public void minimalReturnsTheLowestRate(BigDecimal expectedMinimal, KafkaExchangeRate[] rates) {
		// given
		mockRates(rates);

		// when
		BigDecimal minimalRate = objectUnderTest.getMinimalRate();

		// then
		assertThat(minimalRate).isEqualTo(expectedMinimal);
	}

	Object[] minimalVerificationParameters() {
		return new Object[] {
				row(expected(0.75), outOf(2.5, 0.75, 1.12)),
				row(expected(-4.1), outOf(-4.1, -2, 12.66)),
		};
	}

	@Test
	@Parameters(method = "maximumVerificationParameters")
	public void maximumReturnsTheHighestRate(BigDecimal expectedMaximum, KafkaExchangeRate[] rates) {
		// given
		mockRates(rates);

		// when
		BigDecimal maximumRate = objectUnderTest.getMaximumRate();

		// then
		assertThat(maximumRate).isEqualTo(expectedMaximum);
	}

	Object[] maximumVerificationParameters() {
		return new Object[] {
				row(expected(1.345), outOf(1.345, 1.3449)),
				row(expected(-2.4), outOf(-2.4, -4, -8.33))
		};
	}

	@Test
	@Parameters(method = "averageVerificationParameters")
	public void averageReturnsAverageRate(BigDecimal expectedAverage, KafkaExchangeRate[] rates) {
		// given
		mockRates(rates);

		// when
		BigDecimal averageRate = objectUnderTest.getAverageRate();

		// then
		assertThat(averageRate).isEqualTo(expectedAverage);
	}

	Object[] averageVerificationParameters() {
		return new Object[] {
				row(expected(4.0), outOf(3, 5)),
				row(expected(1.5), outOf(1.5, 1.5, 1.5)),
				row(expected(13.0), outOf(-13, 0, 52))
		};
	}

	private void mockRates(KafkaExchangeRate... rates) {
		@SuppressWarnings("unchecked")
		Iterator<KafkaExchangeRate> mockIterator = mock(Iterator.class);
		NextHelper<KafkaExchangeRate> nextHelper = new NextHelper<>(rates);
		HasNextHelper<KafkaExchangeRate> hasNextHelper = new HasNextHelper<>(rates);

		given(mockIterator.hasNext()).willReturn(hasNextHelper.hasFirst(), hasNextHelper.hasRest());
		given(mockIterator.next()).willReturn(nextHelper.first(), nextHelper.rest());

		given(kafkaRatesMock.iterator()).willReturn(mockIterator);
		given(kafkaRatesMock.size()).willReturn(rates.length);
	}

	private KafkaExchangeRate mockRate(double value) {
		KafkaExchangeRate ker = mock(KafkaExchangeRate.class);
		ExchangeRate er = mock(ExchangeRate.class);
		given(er.getValue()).willReturn(BigDecimal.valueOf(value));
		given(ker.getRate()).willReturn(er);
		return ker;
	}

	private Object[] row(BigDecimal expected, KafkaExchangeRate[] rates) {
		return new Object[] { expected, rates };
	}

	private BigDecimal expected(double value) {
		return BigDecimal.valueOf(value);
	}

	private KafkaExchangeRate[] outOf(double... values) {
		KafkaExchangeRate[] rates = new KafkaExchangeRate[values.length];
		for (int i = 0; i < values.length; i++) {
			rates[i] = mockRate(values[i]);
		}
		return rates;
	}

	private class NextHelper<T> {
		private final T[] elements;

		private NextHelper(T[] elements) {
			this.elements = elements;
		}

		private T first() {
			return elements[0];
		}

		private T[] rest() {
			return copyOfRange(elements, 1, elements.length);
		}
	}

	private class HasNextHelper<T> {
		private final T[] elements;

		private HasNextHelper(T[] elements) {
			this.elements = elements;
		}

		private Boolean hasFirst() {
			return Boolean.TRUE;
		}

		private Boolean[] hasRest() {
			Boolean[] result = new Boolean[elements.length];
			fill(result, Boolean.TRUE);
			result[elements.length - 1] = Boolean.FALSE;
			return result;
		}
	}
}
