package com.taurus.cera.security;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.taurus.cera.configuration.SecurityConfiguration;
import com.taurus.cera.controller.HomeController;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes = SecurityConfiguration.class)
public class SecurityIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	private HomeController homeController;

	private MockMvc mvc;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	@Test
	@WithAnonymousUser
	public void staticResourcesArePubliclyAccessible() throws Exception {
		mvc.perform(get("/css/style.css")).andExpect(status().isOk());
		mvc.perform(get("/favicon.ico")).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	public void anonymousUserCanAccessHomePage() throws Exception {
		mvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	@WithAnonymousUser
	public void actuatorIsSecured() throws Exception {
		mvc.perform(get("/actuator")).andExpect(status().isUnauthorized());
	}

	@Test
	@WithUserDetails(value = "cera")
	public void adminCanAccessActuator() throws Exception {
		mvc.perform(get("/actuator")).andExpect(status().isOk());
	}
}
