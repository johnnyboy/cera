function request(url) {
	if (isHeapDumpRequest(url)) {
		download(url);
	} else {
		ajaxRequest(url);
	}
}

function isHeapDumpRequest(url) {
	return url.indexOf('heapdump') !== -1;
}

function download(url) {
	window.location = url;
	display("Binary");
}

function ajaxRequest(url) {
	jQuery.get(url, callback);
}

function callback(response) {
	display(prettyPrint(response));
}

function prettyPrint(response) {
	var replacer = null;
	var indentation = 2;
	return JSON.stringify(response, replacer, indentation);
}

function display(text) {
	jQuery("#endpoint-response").val(text);
}