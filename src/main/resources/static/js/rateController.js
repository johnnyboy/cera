function request(servicePair) {
	var baseUrl = window.location.href;
	var targetUrl = baseUrl.concat("/").concat(servicePair);
	ajaxRequest(targetUrl);
}

function ajaxRequest(url) {
	jQuery.get(url, callback);
}

function callback(response) {
	update(response);
}

function update(response) {
	jQuery("#exchange-rate").html(response);
}