package com.taurus.cera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.taurus.cera.configuration.ErrorConfiguration;
import com.taurus.cera.configuration.ExchangeRateServiceConfiguration;
import com.taurus.cera.configuration.InMemoryStorageServiceConfiguration;
import com.taurus.cera.configuration.KafkaConfiguration;
import com.taurus.cera.configuration.SecurityConfiguration;

@SpringBootApplication
@Import(value = { KafkaConfiguration.class, ExchangeRateServiceConfiguration.class,
		InMemoryStorageServiceConfiguration.class, SecurityConfiguration.class, ErrorConfiguration.class })
public class CeraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CeraApplication.class, args);
	}

}
