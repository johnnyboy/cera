package com.taurus.cera.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.taurus.cera.model.CurrencyCode;

public class Menu {

	public MenuItem getCera() {
		return new MenuItem("CERA", Links.CERA);
	}

	public MenuItem getHome() {
		return new MenuItem("Home", Links.HOME);
	}

	public MenuItem getWebRates() {
		return new MenuItem("Web rates", Links.WEB_RATES);
	}

	public MenuItem getIndividualWebRates() {
		return new MenuItem("Individual", Links.WEB_RATES_INDIVIDUAL);
	}

	public MenuItem getAggregatedWebRates() {
		return new MenuItem("Aggregated", Links.WEB_RATES_AGGREGATED);
	}

	public Collection<MenuItem> getWebRateItems() {
		return buildRatesMenuItems(Links.WEB_RATES_AGGREGATED);
	}

	public MenuItem getKafkaRates() {
		return new MenuItem("Kafka rates", Links.KAFKA_RATES);
	}

	public Collection<MenuItem> getKafkaRateItems() {
		return buildRatesMenuItems(Links.KAFKA_RATES);
	}

	public MenuItem getActuator() {
		return new MenuItem("Actuator", Links.ACTUATOR);
	}

	public Collection<MenuItem> getActuatorItems() {
		return buildActuatorMenuItems(Links.ACTUATOR);
	}

	public MenuItem getAbout() {
		return new MenuItem("About", Links.ABOUT);
	}

	private Collection<MenuItem> buildRatesMenuItems(String parentUrl) {
		Collection<MenuItem> items = new ArrayList<>(CurrencyCode.count());
		for (CurrencyCode code : CurrencyCode.values()) {
			String childUrl = code.name().toLowerCase();
			MenuItem item = new MenuItem(code.title(), parentUrl, childUrl);
			items.add(item);
		}
		return items;
	}

	private Collection<MenuItem> buildActuatorMenuItems(String parentUrl) {
		return Arrays.asList(
				new MenuItem("Health", parentUrl, "health"),
				new MenuItem("Auto config", parentUrl, "autoconfig"),
				new MenuItem("Config properties", parentUrl, "configprops"),
				new MenuItem("Mappings", parentUrl, "mappings"),
				new MenuItem("Beans", parentUrl, "beans"),
				new MenuItem("Metrics", parentUrl, "metrics"),
				new MenuItem("Trace", parentUrl, "trace"),
				new MenuItem("Environment", parentUrl, "env"),
				new MenuItem("Heap dump", parentUrl, "heapdump"),
				new MenuItem("Thread dump", parentUrl, "dump"),
				new MenuItem("Info", parentUrl, "info"));
	}
}
