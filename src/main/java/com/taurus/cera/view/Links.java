package com.taurus.cera.view;

public class Links {
	private Links() {
	}

	public static final String HOME = "";

	public static final String CERA = "";

	public static final String WEB_RATES = "rates/web";

	public static final String WEB_RATES_AGGREGATED = "rates/web/aggregated";

	public static final String WEB_RATES_INDIVIDUAL = "rates/web/individual";

	public static final String KAFKA_RATES = "rates/kafka";

	public static final String ACTUATOR = "actuator";

	public static final String ABOUT = "about";
}
