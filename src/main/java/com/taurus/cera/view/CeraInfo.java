package com.taurus.cera.view;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import org.springframework.boot.info.GitProperties;

public class CeraInfo {

	private final GitProperties gitProperties;

	private final boolean isAdminMode;

	private final String applicationVersion;

	public CeraInfo(GitProperties gitProperties, boolean isAdminMode, String applicationVersion) {
		checkNotNull(gitProperties);
		checkNotNull(applicationVersion);
		this.gitProperties = gitProperties;
		this.isAdminMode = isAdminMode;
		this.applicationVersion = applicationVersion;
	}

	public boolean isAdminMode() {
		return isAdminMode;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public String getCommitId() {
		return gitProperties.getShortCommitId();
	}

	public String getBranch() {
		return gitProperties.getBranch();
	}

	public Date getCommitTime() {
		return gitProperties.getCommitTime();
	}

	public String getShortCommitMessage() {
		return gitProperties.get("commit.message.short");
	}

	public String getFullCommitMessage() {
		return gitProperties.get("commit.message.full");
	}

	public String getCommitUserName() {
		return gitProperties.get("commit.user.name");
	}

	public String getCommitUserEmail() {
		return gitProperties.get("commit.user.email");
	}

}
