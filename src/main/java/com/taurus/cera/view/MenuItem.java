package com.taurus.cera.view;

import static com.google.common.base.Preconditions.checkNotNull;

public class MenuItem {

	private final String title;

	private final String url;

	public MenuItem(String title, String parentUrl, String url) {
		this(title, String.format("%s/%s", parentUrl, url));
	}

	public MenuItem(String title, String url) {
		checkNotNull(title);
		checkNotNull(url);
		this.title = title;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

}
