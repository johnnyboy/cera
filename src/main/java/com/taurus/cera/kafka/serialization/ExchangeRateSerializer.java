package com.taurus.cera.kafka.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.taurus.cera.model.ExchangeRate;

@Component
public class ExchangeRateSerializer implements Serializer<ExchangeRate> {

	private static ObjectMapper mapper;

	public ExchangeRateSerializer() {
	}

	@Autowired
	public ExchangeRateSerializer(ObjectMapper mapper) {
		ExchangeRateSerializer.mapper = mapper;
	}

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {

	}

	@Override
	public byte[] serialize(String topic, ExchangeRate data) {
		try {
			return mapper.writeValueAsBytes(data);
		} catch (JsonProcessingException e) {
			LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
		}
		return "".getBytes();
	}

	@Override
	public void close() {

	}

}
