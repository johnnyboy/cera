package com.taurus.cera.kafka.serialization;

import java.io.IOException;
import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taurus.cera.model.ExchangeRate;

@Component
public class ExchangeRateDeserializer implements Deserializer<ExchangeRate> {

	private static ObjectMapper mapper;

	public ExchangeRateDeserializer() {
	}

	@Autowired
	public ExchangeRateDeserializer(ObjectMapper mapper) {
		ExchangeRateDeserializer.mapper = mapper;
	}

	@Override
	public void configure(Map<String, ?> configs, boolean isKey) {

	}

	@Override
	public ExchangeRate deserialize(String topic, byte[] data) {
		try {
			return mapper.readValue(data, ExchangeRate.class);
		} catch (IOException e) {
			LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public void close() {

	}

}
