package com.taurus.cera.kafka.task;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.KafkaExchangeRate;
import com.taurus.cera.service.storage.StorageService;

public class ConsumerTask implements Runnable {

	private final Logger log = LoggerFactory.getLogger(ConsumerTask.class);

	private final KafkaConsumer<String, ExchangeRate> consumer;

	private final long pollInterval;

	private final Collection<String> topics;

	private final StorageService storageService;

	private final RecordToKafkaExchangeRateTransformer recordToKafkaExchangeRateFunction = new RecordToKafkaExchangeRateTransformer();

	private final CountDownLatch shutdownLatch = new CountDownLatch(1);

	@SuppressWarnings("unchecked")
	public ConsumerTask(ApplicationContext context, long pollInterval, Collection<String> topics,
			StorageService storageService) {
		this.pollInterval = pollInterval;
		this.topics = topics;
		this.consumer = context.getBean(KafkaConsumer.class);
		this.storageService = storageService;
	}

	@Override
	public void run() {
		try {
			consumer.subscribe(topics);
			consumeUntilWokenUp();
		} catch (WakeupException we) {
			log.trace("Waking up consumer");
		} finally {
			consumer.close();
			shutdownLatch.countDown();
		}

	}

	/**
	 * Consumes records eternally, unless somebody calls {@link #shutdown()} and
	 * triggers a {@link org.apache.kafka.common.errors.WakeupException}
	 * 
	 * @throws WakeupException
	 *             - whenever {@link #shutdown()} method is called
	 */
	private void consumeUntilWokenUp() throws WakeupException {
		while (true) {
			ConsumerRecords<String, ExchangeRate> records = consumer.poll(pollInterval);
			if (records.isEmpty()) {
				continue;
			}
			logRecords(records);
			storageService.save(transform(records));
		}
	}

	private Collection<KafkaExchangeRate> transform(ConsumerRecords<String, ExchangeRate> records) {
		return Lists.newArrayList(Iterables.transform(records, recordToKafkaExchangeRateFunction));
	}

	private void logRecords(ConsumerRecords<String, ExchangeRate> records) {
		if (!log.isTraceEnabled()) {
			return;
		}
		for (ConsumerRecord<String, ExchangeRate> record : records) {
			log.trace("Record : topic {}, message offset {}, exchangeRate {}", record.topic(), record.offset(),
					record.value());
		}
	}

	public void shutdown() {
		consumer.wakeup();
		try {
			shutdownLatch.await();
		} catch (InterruptedException e) {
			log.warn(e.getMessage(), e);
		}
	}

	private class RecordToKafkaExchangeRateTransformer
			implements Function<ConsumerRecord<String, ExchangeRate>, KafkaExchangeRate> {

		@Override
		public KafkaExchangeRate apply(ConsumerRecord<String, ExchangeRate> record) {
			return new KafkaExchangeRate(record.value(), record.offset(), record.partition(), record.timestamp());
		}

	}

}
