package com.taurus.cera.kafka.task;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.service.web.ExchangeRateService;

public class ProducerTask implements Runnable {

	private final Logger log = LoggerFactory.getLogger(ProducerTask.class);

	private final KafkaProducer<String, ExchangeRate> producer;

	private final ExchangeRateService exchangeRateService;

	private final CurrencyPair currencyPair;

	private final String topicName;

	public ProducerTask(KafkaProducer<String, ExchangeRate> producer, ExchangeRateService exchangeRateService,
			CurrencyPair currencyPair, String topicName) {
		this.producer = producer;
		this.exchangeRateService = exchangeRateService;
		this.currencyPair = currencyPair;
		this.topicName = topicName;
	}

	@Override
	public void run() {
		Optional<ExchangeRate> exchangeRate = exchangeRateService.getRate(currencyPair);
		if (!exchangeRate.isPresent())
			return;
		ProducerRecord<String, ExchangeRate> record = new ProducerRecord<>(topicName, exchangeRate.get());
		try {
			producer.send(record);
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
		}
	}
}
