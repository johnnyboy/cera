package com.taurus.cera.kafka.service;

import java.util.Collection;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taurus.cera.kafka.task.ProducerTask;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.service.web.ExchangeRateService;

@Service
public class ScheduledProducerService implements KafkaService {

	private final Logger log = LoggerFactory.getLogger(ScheduledProducerService.class);

	@Value("${kafka.scheduled.producer.initial.delay.seconds:0}")
	private int initialDelay;

	@Value("${kafka.scheduled.producer.delay.seconds:10}")
	private int delay;

	private final ScheduledExecutorService scheduledExecutorService;

	private final KafkaProducer<String, ExchangeRate> kafkaProducer;

	private final Collection<ExchangeRateService> exchangeRateServices;

	private final TopicManager topicManager;

	@Autowired
	public ScheduledProducerService(@Qualifier("producerExecutorService") ScheduledExecutorService scheduledExecutorService,
			KafkaProducer<String, ExchangeRate> kafkaProducer, Collection<ExchangeRateService> exchangeRateServices,
			TopicManager topicManager) {
		this.scheduledExecutorService = scheduledExecutorService;
		this.kafkaProducer = kafkaProducer;
		this.exchangeRateServices = exchangeRateServices;
		this.topicManager = topicManager;
	}

	@Override
	public void start() {
		for (ExchangeRateService service : exchangeRateServices) {
			for (CurrencyPair currencyPair : service.supportedCurrencyPairs()) {
				ProducerTask kafkaProducerTask = new ProducerTask(kafkaProducer, service, currencyPair,
						topicManager.resolveTopicName(currencyPair));
				scheduledExecutorService.scheduleWithFixedDelay(kafkaProducerTask, initialDelay, delay,
						TimeUnit.SECONDS);
			}
		}
		kafkaProducer.flush();
	}

	@Override
	public void stop() {
		try {
			scheduledExecutorService.shutdownNow();
			scheduledExecutorService.awaitTermination(2, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.warn(e.getMessage(), e);
		} finally {
			kafkaProducer.close();
		}
	}
}
