package com.taurus.cera.kafka.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.taurus.cera.kafka.task.ConsumerTask;
import com.taurus.cera.service.storage.StorageService;

@Service
public class PollingConsumerService implements KafkaService {

	private final ExecutorService fixedSizeExecutorService;

	private ConsumerTask[] tasks;

	@Autowired
	public PollingConsumerService(ApplicationContext context,
			@Qualifier("consumerExecutorService") ExecutorService fixedSizeExecutorService, TopicManager topicManager,
			StorageService storageService, @Value("${kafka.number.of.consumers}") int numberOfConsumers,
			@Value("${kafka.consumer.poll.interval.seconds}") long pollInterval) {
		this.fixedSizeExecutorService = fixedSizeExecutorService;
		this.tasks = new ConsumerTask[numberOfConsumers];
		for (int i = 0; i < numberOfConsumers; i++) {
			tasks[i] = new ConsumerTask(context, TimeUnit.SECONDS.toMillis(pollInterval), topicManager.ceraTopics(),
					storageService);
		}

	}

	@Override
	public void start() {
		for (ConsumerTask task : tasks) {
			fixedSizeExecutorService.execute(task);
		}
	}

	@Override
	public void stop() {
		for (ConsumerTask task : tasks) {
			task.shutdown();
		}
		fixedSizeExecutorService.shutdownNow();
	}
}
