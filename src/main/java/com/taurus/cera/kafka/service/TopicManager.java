package com.taurus.cera.kafka.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.CurrencyPairs;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode.Enforced$;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import scala.collection.Seq;

@Service
public class TopicManager {

	private final Logger log = LoggerFactory.getLogger(TopicManager.class);

	@Value("${zookeeper.hosts}")
	private String zookeeperHosts;

	@Value("${zookeeper.session.timeout.milliseconds}")
	private int sessionTimeout;

	@Value("${zookeeper.connection.timeout.milliseconds}")
	private int connectionTimeout;

	@Value("${kafka.cluster.secured}")
	private boolean isClusterSecured;

	@Value("${cera.default.topic.partitions}")
	private int partitions;

	@Value("${cera.default.topic.replication.factor}")
	private int replications;

	public void createMissingTopicsIfAny() {
		ZkClient zkClient = null;
		try {
			zkClient = new ZkClient(zookeeperHosts, sessionTimeout, connectionTimeout, ZKStringSerializer$.MODULE$);
			ZkUtils zkUtils = new ZkUtils(zkClient, new ZkConnection(zookeeperHosts), isClusterSecured);

			Collection<String> requiredTopics = ceraTopics();
			requiredTopics.removeAll(listAllKafkaTopics(zkUtils));
			if (!requiredTopics.isEmpty()) {
				log.info("Creating missing topics {}", requiredTopics);
			}
			for (String topic : requiredTopics) {
				createTopic(zkUtils, topic);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (zkClient != null) {
				zkClient.close();
			}
		}
	}

	private void createTopic(ZkUtils zkUtils, String topicName) throws RuntimeException {
		Properties topicConfiguration = new Properties();
		AdminUtils.createTopic(zkUtils, topicName, partitions, replications, topicConfiguration, Enforced$.MODULE$);
		log.info("Created topic {}", topicName);
	}

	private Collection<String> listAllKafkaTopics(ZkUtils zookeperUtils) {
		Seq<String> scalaTopics = zookeperUtils.getAllTopics();
		Collection<String> topics = new HashSet<>(scalaTopics.size());
		scala.collection.Iterator<String> iterator = scalaTopics.iterator();
		while (iterator.hasNext()) {
			topics.add(iterator.next());
		}
		return topics;
	}

	public Collection<String> ceraTopics() {
		Collection<CurrencyPair> supportedCurrencies = CurrencyPairs.all();
		Collection<String> topics = new HashSet<>(supportedCurrencies.size());
		for (CurrencyPair currencyPair : supportedCurrencies) {
			topics.add(resolveTopicName(currencyPair));
		}
		return topics;
	}

	public String resolveTopicName(CurrencyPair pair) {
		return String.format("%s_%s", pair.getFrom().name(), pair.getTo().name());
	}
}
