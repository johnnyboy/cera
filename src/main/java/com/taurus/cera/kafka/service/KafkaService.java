package com.taurus.cera.kafka.service;

public interface KafkaService {
	
	void start();

	void stop();
}
