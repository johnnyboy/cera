package com.taurus.cera.security;

import java.util.Collections;

import org.springframework.security.core.userdetails.UserDetails;

public enum User {
	CERA_ADMIN("cera", "$2a$10$cCZXp87QV7DUenoxEKXbwuFlll/soejyCpP28HvmGg12nDjEt9lG.", Role.ADMIN), KAFKA_ADMIN("kafka",
			"$2a$10$VpIYHgfSiCJ4Ogds5mlg/.2fw5lj9jjB3O24wAzhjZgozZx2Ip10u", Role.KAFKA);

	private final String userName;

	private final String password;

	private final Role role;

	User(String userName, String password, Role role) {
		this.userName = userName;
		this.password = password;
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public Role getRole() {
		return role;
	}

	public UserDetails userDetails() {
		return new org.springframework.security.core.userdetails.User(getUserName(), getPassword(),
				Collections.singleton(getRole().authority()));
	}

	public static int count() {
		return values().length;
	}
}
