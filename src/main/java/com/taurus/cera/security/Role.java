package com.taurus.cera.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

public enum Role {
	ADMIN, KAFKA;

	public SimpleGrantedAuthority authority() {
		return new SimpleGrantedAuthority(roleName());
	}

	public String roleName() {
		return String.format("%s_%s", "ROLE", name());
	}
}
