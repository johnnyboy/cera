package com.taurus.cera.controller.advice;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.GitProperties;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.samskivert.mustache.Mustache;
import com.taurus.cera.controller.converter.CurrencyCodeConverter;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.security.Role;
import com.taurus.cera.view.CeraInfo;
import com.taurus.cera.view.Layout;
import com.taurus.cera.view.Menu;

@ControllerAdvice
class GlobalControllerAdvice {

	@Autowired
	private Mustache.Compiler compiler;

	@Autowired
	private GitProperties gitProperties;

	@Value("${info.application.version}")
	private String applicationVersion;

	@InitBinder(value = { "from", "to", "base" })
	void dataBinding(WebDataBinder binder) {
		binder.registerCustomEditor(CurrencyCode.class, new CurrencyCodeConverter());
	}

	@ModelAttribute("layout")
	public Mustache.Lambda layout(Map<String, Object> model) {
		return new Layout(compiler);
	}

	@ModelAttribute("baseUrl")
	public String baseUrl(HttpServletRequest req) {
		String baseUrl = String.format("%s://%s:%d%s/", req.getScheme(), req.getServerName(), req.getServerPort(),
				req.getServletContext().getContextPath());
		return baseUrl;
	}

	@ModelAttribute("menu")
	public Menu menu() {
		return new Menu();
	}

	@ModelAttribute("cera")
	public CeraInfo ceraInfo(HttpServletRequest req) {
		boolean isAdminRequest = req.isUserInRole(Role.ADMIN.roleName());
		return new CeraInfo(gitProperties, isAdminRequest, applicationVersion);
	}
}
