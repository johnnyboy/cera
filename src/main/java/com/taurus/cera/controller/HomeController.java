package com.taurus.cera.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.RateSource;
import com.taurus.cera.model.StatisticalExchangeRate;
import com.taurus.cera.service.storage.StorageService;

@Controller
public class HomeController {

	@Autowired
	private StorageService storageService;

	@GetMapping(path = "/")
	public ModelAndView index() {
		if (storageService.isEmpty()) {
			return new ModelAndView("kafkaRates", "averageRates", Collections.emptyList());
		}
		CurrencyCode base = CurrencyCode.forDefaultLocale();
		Collection<StatisticalExchangeRate> statisticalRates = new ArrayList<>(base.pairs().size());
		for (CurrencyPair pair : base.pairs()) {
			statisticalRates.add(new StatisticalExchangeRate(pair, storageService.findBy(pair)));
		}
		return new ModelAndView("kafkaRates", "statisticalRates", statisticalRates);
	}

	@GetMapping(path = "/actuator")
	public ModelAndView actuator() {
		return new ModelAndView("actuator");
	}

	@GetMapping(path = "/about")
	public ModelAndView about() {
		Map<String, Object> contextMap = new HashMap<>(2);
		contextMap.put("rateServices", RateSource.values());
		contextMap.put("currencies", CurrencyCode.values());
		return new ModelAndView("about", contextMap);
	}

}
