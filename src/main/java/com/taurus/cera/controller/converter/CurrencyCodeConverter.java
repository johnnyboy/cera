package com.taurus.cera.controller.converter;

import java.beans.PropertyEditorSupport;

import com.taurus.cera.model.CurrencyCode;

public class CurrencyCodeConverter extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		CurrencyCode code = CurrencyCode.valueOf(text.toUpperCase());
		setValue(code);
	}
}
