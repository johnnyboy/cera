package com.taurus.cera.controller;

import static com.taurus.cera.model.CurrencyPair.of;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;
import com.taurus.cera.service.web.ExchangeRateService;

@Controller
@RequestMapping(path = "/rates/web/individual")
public class IndividualServiceController {

	@Autowired
	private Collection<ExchangeRateService> services;

	@GetMapping
	public ModelAndView home() {
		return new ModelAndView("individualRates", "services", services);
	}

	@GetMapping(path = "/{serviceUrlName}/{from}-{to}")
	public ModelAndView serviceSpecificRate(@PathVariable("serviceUrlName") String urlName,
			@PathVariable("from") CurrencyCode from, @PathVariable("to") CurrencyCode to) {
		RateSource source = RateSource.byUrlName(urlName);
		ExchangeRateService service = lookupByRateSource(source);
		CurrencyPair pair = of(from, to);
		ExchangeRate exchangeRate = service.getRate(pair).orNull();
		
		Map<String, Object> context = buildContextMap(from, to, source, exchangeRate);
		
		return new ModelAndView("individualRate", context);
	}

	private Map<String, Object> buildContextMap(CurrencyCode from, CurrencyCode to, RateSource source,
			ExchangeRate exchangeRate) {
		Map<String, Object> context = new HashMap<>(2);
		context.put("rate", exchangeRate);
		context.put("url", String.format("%s/%s-%s", source.urlName(), from, to));
		return context;
	}

	@GetMapping(path = "/{serviceUrlName}/{from}-{to}/json", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ExchangeRate serviceSpecificRateJSON(@PathVariable("serviceUrlName") String urlName,
			@PathVariable("from") CurrencyCode from, @PathVariable("to") CurrencyCode to) {
		RateSource source = RateSource.byUrlName(urlName);
		ExchangeRateService service = lookupByRateSource(source);
		CurrencyPair pair = of(from, to);
		ExchangeRate exchangeRate = service.getRate(pair).orNull();
		return exchangeRate;
	}

	private ExchangeRateService lookupByRateSource(RateSource source) {
		for (ExchangeRateService service : services) {
			if (source.serviceClass() == service.getClass()) {
				return service;
			}
		}
		throw new IllegalArgumentException(
				String.format("No ExchangeRateService implementation for RateSource %s", source));
	}
}
