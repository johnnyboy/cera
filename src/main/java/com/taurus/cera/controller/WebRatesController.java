package com.taurus.cera.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.service.web.AggregatedExchangeRateService;

@Controller
@RequestMapping(path = "/rates/web/aggregated")
public class WebRatesController {

	@Autowired
	private AggregatedExchangeRateService aggregatedRateService;

	@GetMapping(path = "/{base}")
	public ModelAndView baseRates(@PathVariable("base") CurrencyCode base) {
		return new ModelAndView("webRates", "webRates", aggregatedRateService.getBaseRates(base));
	}

}
