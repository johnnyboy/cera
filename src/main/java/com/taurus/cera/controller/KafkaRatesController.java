package com.taurus.cera.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.StatisticalExchangeRate;
import com.taurus.cera.service.storage.StorageService;

@Controller
@RequestMapping(path = "/rates/kafka")
public class KafkaRatesController {

	@Autowired
	private StorageService storageService;

	@GetMapping(path = "/{base}")
	public ModelAndView getBaseRates(@PathVariable("base") CurrencyCode base) {
		Collection<StatisticalExchangeRate> statisticalRates = new ArrayList<>(base.pairs().size());
		for (CurrencyPair pair : base.pairs()) {
			statisticalRates.add(new StatisticalExchangeRate(pair, storageService.findBy(pair)));
		}
		return new ModelAndView("kafkaRates", "statisticalRates", statisticalRates);
	}
}
