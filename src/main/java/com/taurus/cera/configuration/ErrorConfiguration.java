package com.taurus.cera.configuration;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorConfiguration {

	@Bean
	ErrorProperties errorProperties() {
		return new ErrorProperties();
	}
}
