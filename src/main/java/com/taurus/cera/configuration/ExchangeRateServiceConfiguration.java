package com.taurus.cera.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

@Configuration
@PropertySource(value = "classpath:/config/exchange.rate.service.providers.properties")
public class ExchangeRateServiceConfiguration {

	@Value("${rest.template.connection.timeout}")
	private int restConnectTimeout;

	@Value("${rest.template.read.timeout}")
	private int restReadTimeout;

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		mapper.registerModule(new JodaModule());
		return mapper;
	}
	
	@Bean
	public MappingJackson2HttpMessageConverter jacksonHttpConverter() {
		return new MappingJackson2HttpMessageConverter(objectMapper());
	}

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate template = new RestTemplate(clientHttpRequestFactory());
		return template;
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(restConnectTimeout);
		clientHttpRequestFactory.setReadTimeout(restReadTimeout);
		return clientHttpRequestFactory;
	}
}
