package com.taurus.cera.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:/config/in.memory.storage.properties")
public class InMemoryStorageServiceConfiguration {

}
