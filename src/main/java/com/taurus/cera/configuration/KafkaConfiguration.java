package com.taurus.cera.configuration;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.taurus.cera.model.CurrencyPairs;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.service.web.ExchangeRateService;

@Configuration
@PropertySource(value = { "classpath:/config/kafka.scheduled.producer.service.properties",
		"classpath:/config/kafka.polling.consumer.service.properties",
		"classpath:/config/kafka.topic.manager.properties" })
public class KafkaConfiguration {

	@Value("${kafka.number.of.consumers}")
	private int numberOfConsumers;

	@Autowired
	private Collection<ExchangeRateService> exchangeRateServices;

	@Bean
	public KafkaProducer<String, ExchangeRate> kafkaProducer() {
		Properties properties = loadProperties("kafka.producer.properties");
		return new KafkaProducer<String, ExchangeRate>(properties);
	}

	@Bean
	@Scope(scopeName = SCOPE_PROTOTYPE)
	public KafkaConsumer<String, ExchangeRate> kafkaConsumer() {
		Properties properties = loadProperties("kafka.consumer.properties");
		return new KafkaConsumer<String, ExchangeRate>(properties);
	}

	private Properties loadProperties(String fileName) {
		final String configPackage = "/config/";
		Resource resource = new ClassPathResource(configPackage.concat(fileName));
		try {
			return PropertiesLoaderUtils.loadProperties(resource);
		} catch (IOException e) {
			throw new Error(e.getMessage(), e);
		}
	}

	@Bean
	@Qualifier("producerExecutorService")
	public ScheduledExecutorService scheduledExecutor() {
		return Executors.newScheduledThreadPool(numberOfProducerThreads());
	}

	private int numberOfProducerThreads() {
		return CurrencyPairs.all().size() * exchangeRateServices.size();
	}

	@Bean
	@Qualifier("consumerExecutorService")
	public ExecutorService fixedSizeExecutor() {
		return Executors.newFixedThreadPool(numberOfConsumers);
	}
}
