package com.taurus.cera.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.taurus.cera.kafka.service.PollingConsumerService;
import com.taurus.cera.kafka.service.ScheduledProducerService;
import com.taurus.cera.kafka.service.TopicManager;

@Component
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {
	private final Logger log = LoggerFactory.getLogger(StartupListener.class);

	private final TopicManager topicManager;

	private final ScheduledProducerService producerService;

	private final PollingConsumerService consumerService;

	@Autowired
	public StartupListener(TopicManager topicManager, ScheduledProducerService producerService,
			PollingConsumerService consumerService) {
		this.topicManager = topicManager;
		this.producerService = producerService;
		this.consumerService = consumerService;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("Checking cera topics");
		topicManager.createMissingTopicsIfAny();

		log.debug("Starting Kafka producer");
		producerService.start();

		log.debug("Starting Kafka consumer");
		consumerService.start();
	}

}
