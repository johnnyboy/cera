package com.taurus.cera.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import com.taurus.cera.kafka.service.PollingConsumerService;
import com.taurus.cera.kafka.service.ScheduledProducerService;
import com.taurus.cera.service.storage.StorageService;

@Component
public class ShutdownListener implements ApplicationListener<ContextClosedEvent> {

	private final ScheduledProducerService producerService;

	private final PollingConsumerService consumerService;

	private final StorageService storageService;

	@Autowired
	public ShutdownListener(ScheduledProducerService producerService, PollingConsumerService consumerService,
			StorageService storageService) {
		this.producerService = producerService;
		this.consumerService = consumerService;
		this.storageService = storageService;
	}

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		consumerService.stop();
		producerService.stop();
		storageService.size();
	}

}
