package com.taurus.cera.service.storage;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singleton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Predicate;
import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.CurrencyPairs;
import com.taurus.cera.model.KafkaExchangeRate;

@Service
public class InMemoryStorageService implements StorageService {

	@Value("${in.memory.storage.max.rates.per.key}")
	private int MAX_RATES_PER_KEY;

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Map<CurrencyPair, Collection<KafkaExchangeRate>> map = new HashMap<>(CurrencyPairs.all().size());

	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	@Override
	public void save(KafkaExchangeRate rate) {
		save(singleton(rate));
	}

	@Override
	public void save(Collection<KafkaExchangeRate> rates) {
		checkNotNull(rates, "rates cannot be null");
		lock.writeLock().lock();
		for (KafkaExchangeRate rate : rates) {
			Collection<KafkaExchangeRate> savedRates = emptyWithCapacityIfNone(rate.key());
			savedRates.add(rate);
			map.put(rate.key(), savedRates);
			log.trace("Key {} : updated rates {}", rate.key(), savedRates);
		}
		lock.writeLock().unlock();
	}

	private Collection<KafkaExchangeRate> emptyWithCapacityIfNone(CurrencyPair key) {
		Collection<KafkaExchangeRate> savedRates = map.get(key);
		if (savedRates == null) {
			log.trace("No saved rates for {} yet, instantiating collection with capacity {}", key, MAX_RATES_PER_KEY);
			savedRates = EvictingQueue.create(MAX_RATES_PER_KEY);
		}
		return savedRates;
	}

	@Override
	public Collection<KafkaExchangeRate> findBy(CurrencyPair key) {
		return findBy(singleton(key));
	}

	@Override
	public Collection<KafkaExchangeRate> findBy(Collection<CurrencyPair> keys) {
		checkNotNull(keys, "pairs cannot be null");
		lock.readLock().lock();
		Collection<KafkaExchangeRate> rates = new ArrayList<>(MAX_RATES_PER_KEY * keys.size());
		for (CurrencyPair pair : keys) {
			Collection<KafkaExchangeRate> savedRates = emptyIfNone(pair);
			rates.addAll(savedRates);
		}
		lock.readLock().unlock();
		log.trace("{} found by {}", rates, keys);
		return rates;
	}

	private Collection<KafkaExchangeRate> emptyIfNone(CurrencyPair key) {
		Collection<KafkaExchangeRate> savedRates = map.get(key);
		if (savedRates == null) {
			log.trace("No saved rates for {} yet, returning empty collection", key);
			savedRates = Collections.emptyList();
		}
		return savedRates;
	}

	@Override
	public Collection<KafkaExchangeRate> findBy(CurrencyPair key, Predicate<KafkaExchangeRate> predicate) {
		return findBy(singleton(key), predicate);
	}

	@Override
	public Collection<KafkaExchangeRate> findBy(Collection<CurrencyPair> keys, Predicate<KafkaExchangeRate> predicate) {
		checkNotNull(keys, "pairs cannot be null");
		checkNotNull(predicate, "predicate cannot be null");
		List<KafkaExchangeRate> result = Lists.newArrayList(Iterables.filter(findBy(keys), predicate));
		log.trace("Found {} by {} and {}", result, keys, predicate);
		return result;
	}

	@Override
	public int size() {
		lock.readLock().lock();
		int total = 0;
		for (CurrencyPair key : map.keySet()) {
			log.trace("Key {}, rates in storage {}", key, map.get(key).size());
			total += map.get(key).size();
		}
		lock.readLock().unlock();
		log.trace("{} elements in storage", total);
		return total;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public void clear() {
		lock.writeLock().lock();
		log.trace("Storage cleared");
		map.clear();
		lock.writeLock().unlock();
	}
}
