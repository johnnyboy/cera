package com.taurus.cera.service.storage;

import java.util.Collection;

import com.google.common.base.Predicate;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.KafkaExchangeRate;

public interface StorageService {

	void save(KafkaExchangeRate rate);

	void save(Collection<KafkaExchangeRate> rates);

	Collection<KafkaExchangeRate> findBy(CurrencyPair key);

	Collection<KafkaExchangeRate> findBy(Collection<CurrencyPair> keys);

	Collection<KafkaExchangeRate> findBy(CurrencyPair key, Predicate<KafkaExchangeRate> predicate);

	Collection<KafkaExchangeRate> findBy(Collection<CurrencyPair> keys, Predicate<KafkaExchangeRate> predicate);

	int size();

	void clear();
	
	boolean isEmpty();
}
