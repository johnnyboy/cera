package com.taurus.cera.service.web;

import static com.taurus.cera.model.CurrencyCode.valueOf;
import static com.taurus.cera.model.CurrencyPair.of;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.CurrencyPairs;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;

@Service("fixerIOService")
public class FixerIORateService extends AbstractExchangeRateService {

	@Value("${fixer.io.api.url}")
	private String BASE_URL;

	@Override
	public RateSource getRateSource() {
		return RateSource.EUROPEAN_CENTRAL_BANK_FIXER_IO;
	}

	@Override
	protected String baseURL() {
		return BASE_URL;
	}

	@Override
	protected ExchangeRate parse(String response) throws JsonProcessingException, IOException {
		JsonNode root = objectMapper.readTree(response);
		CurrencyCode from = valueOf(root.findValue("base").asText());
		Entry<String, JsonNode> rates = assertRatesNodeExist(root);
		CurrencyCode to = valueOf(rates.getKey());
		double rate = rates.getValue().asDouble();
		ExchangeRate exchangeRate = new ExchangeRate(of(from, to), BigDecimal.valueOf(rate), DateTime.now(),
				getRateSource());
		return exchangeRate;
	}

	private Entry<String, JsonNode> assertRatesNodeExist(JsonNode root) throws IOException {
		try {
			return root.findPath("rates").fields().next();
		} catch (NoSuchElementException nsee) {
			throw new IOException(nsee.getMessage(), nsee);
		}
	}

	@Override
	public Collection<CurrencyPair> supportedCurrencyPairs() {
		return CurrencyPairs.allBut(CurrencyCode.UAH);
	}

	@Override
	public Collection<CurrencyCode> supportedCurrencyCodes() {
		return CurrencyCode.allExcept(CurrencyCode.UAH);
	}
}
