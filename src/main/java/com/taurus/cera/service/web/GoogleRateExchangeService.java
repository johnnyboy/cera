package com.taurus.cera.service.web;

import java.io.IOException;
import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;

@Service("googleService")
public class GoogleRateExchangeService extends AbstractExchangeRateService {

	@Value("${google.rate.exchange.api.url}")
	private String BASE_URL;

	@Override
	protected String baseURL() {
		return BASE_URL;
	}

	@Override
	protected ExchangeRate parse(String response) throws JsonProcessingException, IOException {
			JsonNode root = objectMapper.readTree(response);
			String from = root.findValue("from").asText();
			String to = root.findValue("to").asText();
			double rate = root.findValue("rate").asDouble();
			ExchangeRate exchangeRate = new ExchangeRate(
					new CurrencyPair(CurrencyCode.valueOf(from), CurrencyCode.valueOf(to)), BigDecimal.valueOf(rate),
					DateTime.now(), getRateSource());
			return exchangeRate;
	}

	@Override
	public RateSource getRateSource() {
		return RateSource.GOOGLE_RATE_EXCHANGE;
	}

}
