package com.taurus.cera.service.web;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;

@Service
public class AggregatedExchangeRateService {

	@Autowired
	private Collection<ExchangeRateService> services;

	public Collection<ExchangeRate> getRates(Collection<CurrencyPair> pairs) {
		Collection<Optional<ExchangeRate>> rates = new ArrayList<>(services.size() * pairs.size());
		for (CurrencyPair pair : pairs) {
			rates.addAll(ratesPerPair(pair));
		}
		return Lists.newArrayList(Optional.presentInstances(rates));
	}

	public Collection<ExchangeRate> getBaseRates(CurrencyCode base) {
		return getRates(base.pairs());
	}

	private Collection<Optional<ExchangeRate>> ratesPerPair(CurrencyPair pair) {
		Collection<Optional<ExchangeRate>> rates = new ArrayList<>(services.size());
		for (ExchangeRateService service : services) {
			rates.add(service.getRate(pair));
		}
		return rates;
	}
}
