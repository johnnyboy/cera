package com.taurus.cera.service.web;

import java.io.IOException;
import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;

@Service("yahooService")
public class YahooExchangeRateService extends AbstractExchangeRateService {

	@Value("${yahoo.finance.api.url}")
	private String BASE_URL;

	@Override
	public ExchangeRate parse(String response) throws JsonProcessingException, IOException {
		JsonNode root = objectMapper.readTree(response);
		String pair = root.findValue("Name").asText();
		double rate = root.findValue("Rate").asDouble();
		ExchangeRate exchangeRate = new ExchangeRate(parseCurrencyPair(pair), BigDecimal.valueOf(rate), DateTime.now(),
				getRateSource());
		return exchangeRate;
	}

	private CurrencyPair parseCurrencyPair(String pairName) {
		String[] elements = pairName.split("/");
		CurrencyCode from = CurrencyCode.valueOf(elements[0]);
		CurrencyCode to = CurrencyCode.valueOf(elements[1]);
		return CurrencyPair.of(from, to);
	}

	@Override
	protected String baseURL() {
		return BASE_URL;
	}

	@Override
	public RateSource getRateSource() {
		return RateSource.YAHOO;
	}
}
