package com.taurus.cera.service.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.taurus.cera.model.CurrencyPair;

@Component
class URLResolver {
	
	private final String fromPattern;

	private final String toPattern;

	@Autowired
	URLResolver(@Value("@FROM@") String fromPattern, @Value("@TO@") String toPattern) {
		this.fromPattern = fromPattern;
		this.toPattern = toPattern;
	}

	String resolve(String baseUrl, CurrencyPair pair) {
		return baseUrl.replaceAll(fromPattern, pair.getFrom().toString()).replaceAll(toPattern,
				pair.getTo().toString());
	}
}
