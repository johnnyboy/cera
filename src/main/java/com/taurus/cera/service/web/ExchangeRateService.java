package com.taurus.cera.service.web;

import java.util.Collection;

import com.google.common.base.Optional;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;

public interface ExchangeRateService {

	Optional<ExchangeRate> getRate(CurrencyPair currencyPair);

	RateSource getRateSource();

	boolean isAlive();

	Collection<CurrencyPair> supportedCurrencyPairs();
	
	Collection<CurrencyCode> supportedCurrencyCodes();
}
