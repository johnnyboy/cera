package com.taurus.cera.service.web;

import java.io.IOException;
import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.ExchangeRate;
import com.taurus.cera.model.RateSource;

@Service("currencyConverterApiService")
public class CurrencyConverterApiService extends AbstractExchangeRateService {

	@Value("${currency.converter.api.url}")
	private String BASE_URL;

	@Override
	protected String baseURL() {
		return BASE_URL;
	}

	@Override
	public ExchangeRate parse(String response) throws JsonProcessingException, IOException{
		JsonNode root = objectMapper.readTree(response);
		String currencyPair = root.findValue("id").asText();
		double rate = root.findValue("val").asDouble();
		ExchangeRate exchangeRate = new ExchangeRate(parseCurrencyPair(currencyPair), BigDecimal.valueOf(rate),
				DateTime.now(), getRateSource());
		return exchangeRate;
	}

	private CurrencyPair parseCurrencyPair(String pairName) {
		String[] elements = pairName.split("_");
		CurrencyCode from = CurrencyCode.valueOf(elements[0]);
		CurrencyCode to = CurrencyCode.valueOf(elements[1]);
		return new CurrencyPair(from, to);
	}

	@Override
	public RateSource getRateSource() {
		return RateSource.CURRENCY_CONVERTER_API;
	}

}
