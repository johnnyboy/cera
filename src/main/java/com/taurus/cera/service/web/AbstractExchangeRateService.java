package com.taurus.cera.service.web;

import static com.taurus.cera.model.CurrencyCode.EUR;
import static com.taurus.cera.model.CurrencyCode.USD;
import static com.taurus.cera.model.CurrencyPair.of;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Optional;
import com.taurus.cera.model.CurrencyCode;
import com.taurus.cera.model.CurrencyPair;
import com.taurus.cera.model.CurrencyPairs;
import com.taurus.cera.model.ExchangeRate;

public abstract class AbstractExchangeRateService implements ExchangeRateService {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected RestTemplate restTemplate;

	@Autowired
	protected URLResolver urlResolver;

	@Override
	public Optional<ExchangeRate> getRate(CurrencyPair currencyPair) {
		log.info("Fetching {} exchange rate", currencyPair);
		try {
			String rawResponse = restTemplate.getForObject(buildURL(currencyPair), String.class);
			ExchangeRate rate = parse(rawResponse);
			return Optional.of(rate);
		} catch (RestClientException | IOException e) {
			log.warn("Failed to fetch {} exchange rate: {}", currencyPair, e.getMessage());
			return Optional.absent();
		}
	}

	protected abstract String baseURL();

	protected String buildURL(CurrencyPair currencyPair) {
		return urlResolver.resolve(baseURL(), currencyPair);
	}

	protected abstract ExchangeRate parse(String response) throws JsonProcessingException, IOException;

	@Override
	public boolean isAlive() {
		try {
			ResponseEntity<String> response = restTemplate.exchange(buildURL(of(USD, EUR)), HttpMethod.GET, null,
					String.class);
			return response.hasBody();
		} catch (RestClientException rse) {
			return false;
		}
	}

	@Override
	public Collection<CurrencyPair> supportedCurrencyPairs() {
		return CurrencyPairs.all();
	}

	@Override
	public Collection<CurrencyCode> supportedCurrencyCodes() {
		return Arrays.asList(CurrencyCode.values());
	}
}
