package com.taurus.cera.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KafkaExchangeRate implements Comparable<KafkaExchangeRate> {

	private final ExchangeRate rate;

	private final long offset;

	private final int partition;

	private final DateTime brokerTimestamp;

	@JsonCreator
	public KafkaExchangeRate(@JsonProperty("exchangeRate") ExchangeRate rate, @JsonProperty("offset") long offset,
			@JsonProperty("partition") int partition, @JsonProperty("brokerTimestamp") long brokerTimestamp) {
		checkNotNull(rate);
		this.rate = rate;
		this.offset = offset;
		this.partition = partition;
		this.brokerTimestamp = new DateTime(brokerTimestamp, DateTimeZone.UTC);
	}

	@Override
	public int compareTo(KafkaExchangeRate other) {
		return rate.compareTo(other.rate);
	}

	@JsonIgnore
	public CurrencyPair key() {
		return rate.getCurrencyPair();
	}

	public ExchangeRate getRate() {
		return rate;
	}

	public long getOffset() {
		return offset;
	}

	public int getPartition() {
		return partition;
	}

	public DateTime getBrokerTimestamp() {
		return brokerTimestamp;
	}

	@JsonIgnore
	public String getFormattedBrokerTimestamp() {
		return brokerTimestamp.toString(ExchangeRate.TIMESTAMP_PATTERN);
	}

	@Override
	public int hashCode() {
		return Objects.hash(offset, partition);
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!this.getClass().equals(obj.getClass())) {
			return false;
		}
		KafkaExchangeRate other = (KafkaExchangeRate) obj;
		return Objects.equals(offset, other.offset) && Objects.equals(partition, other.partition);
	}

	@Override
	public String toString() {
		return String.format("KafkaExchangeRate offset %d, partition %d, broker timestamp %s, exchangeRate %s", offset,
				partition, brokerTimestamp, rate);
	}

}
