package com.taurus.cera.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public enum CurrencyCode {
	CNY, EUR, GBP, PLN, UAH, USD;

	public Currency currency() {
		return Currency.getInstance(name());
	}

	public String title() {
		return currency().getDisplayName(Locale.ENGLISH);
	}

	public Collection<CurrencyPair> pairs() {
		Collection<CurrencyCode> complementers = allExcept(this);
		List<CurrencyPair> pairs = new ArrayList<>(complementers.size());
		for (CurrencyCode complement : complementers)
			pairs.add(new CurrencyPair(this, complement));
		return pairs;
	}

	public static Collection<CurrencyCode> allExcept(CurrencyCode... codes) {
		List<CurrencyCode> list = new ArrayList<>(values().length);
		Collections.addAll(list, values());
		list.removeAll(Arrays.asList(codes));
		return list;
	}

	public static int count() {
		return values().length;
	}

	public static CurrencyCode forDefaultLocale() {
		CurrencyCode defaultCode = CurrencyCode.USD;
		Currency localeBasedCurrency = localeCurrency(Locale.getDefault());
		if (localeBasedCurrency == null) {
			return defaultCode;
		}
		for (CurrencyCode code : values()) {
			if (code.currency().equals(localeBasedCurrency)) {
				return code;
			}
		}
		return defaultCode;
	}

	private static Currency localeCurrency(Locale locale) {
		try {
			return Currency.getInstance(locale);
		} catch (NullPointerException | IllegalArgumentException e) {
			return null;
		}
	}
}
