package com.taurus.cera.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyPair implements Comparable<CurrencyPair> {
	private final CurrencyCode from;
	private final CurrencyCode to;

	@JsonCreator
	public CurrencyPair(@JsonProperty("from") CurrencyCode from, @JsonProperty("to") CurrencyCode to) {
		checkNotNull(from);
		checkNotNull(to);
		this.from = from;
		this.to = to;
	}

	public CurrencyCode getFrom() {
		return from;
	}

	public CurrencyCode getTo() {
		return to;
	}

	@Override
	public int hashCode() {
		return Objects.hash(from, to);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyPair other = (CurrencyPair) obj;
		return (from == other.from) && (to == other.to);
	}

	public boolean contains(CurrencyCode code) {
		return to == code || from == code;
	}

	@Override
	public String toString() {
		return String.format("%s -> %s", from.name(), to.name());
	}

	public static CurrencyPair of(CurrencyCode from, CurrencyCode to) {
		return new CurrencyPair(from, to);
	}

	@Override
	public int compareTo(CurrencyPair other) {
		int fromComparison = from.compareTo(other.from);
		if (fromComparison != 0) {
			return fromComparison;
		}
		return to.compareTo(other.to);
	}
}
