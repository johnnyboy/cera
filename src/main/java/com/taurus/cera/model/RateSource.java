package com.taurus.cera.model;

import com.taurus.cera.service.web.CurrencyConverterApiService;
import com.taurus.cera.service.web.ExchangeRateService;
import com.taurus.cera.service.web.FixerIORateService;
import com.taurus.cera.service.web.GoogleRateExchangeService;
import com.taurus.cera.service.web.YahooExchangeRateService;

public enum RateSource {

	YAHOO {
		@Override
		public String urlName() {
			return "yahoo";
		}

		@Override
		public Class<? extends ExchangeRateService> serviceClass() {
			return YahooExchangeRateService.class;
		}
	},
	GOOGLE_RATE_EXCHANGE {
		@Override
		public String urlName() {
			return "google";
		}

		@Override
		public Class<? extends ExchangeRateService> serviceClass() {
			return GoogleRateExchangeService.class;
		}
	},
	EUROPEAN_CENTRAL_BANK_FIXER_IO {
		@Override
		public String urlName() {
			return "ecb";
		}

		@Override
		public Class<? extends ExchangeRateService> serviceClass() {
			return FixerIORateService.class;
		}
	},
	CURRENCY_CONVERTER_API {
		@Override
		public String urlName() {
			return "cca";
		}

		@Override
		public Class<? extends ExchangeRateService> serviceClass() {
			return CurrencyConverterApiService.class;
		}
	};

	public abstract String urlName();

	public abstract Class<? extends ExchangeRateService> serviceClass();
	
	public static RateSource byUrlName(String urlName) {
		for (RateSource source : RateSource.values()) {
			if (source.urlName().equalsIgnoreCase(urlName)) {
				return source;
			}
		}
		throw new IllegalArgumentException(String.format("No RateSource for urlName %s", urlName));
	}
}
