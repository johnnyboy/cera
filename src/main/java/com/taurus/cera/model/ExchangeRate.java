package com.taurus.cera.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Objects;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExchangeRate implements Comparable<ExchangeRate> {
	static final String TIMESTAMP_PATTERN = "dd-MM-yyyy HH:mm:ss.SSS [z]";
	private final CurrencyPair currencyPair;
	private final BigDecimal value;
	private final DateTime timestamp;
	private final RateSource source;

	@JsonCreator
	public ExchangeRate(@JsonProperty("currencyPair") CurrencyPair currencyPair,
			@JsonProperty("value") BigDecimal value,
			@JsonProperty("timestamp") DateTime timestamp, @JsonProperty("source") RateSource source) {
		checkNotNull(currencyPair);
		checkNotNull(value);
		checkNotNull(timestamp);
		checkNotNull(source);
		this.currencyPair = currencyPair;
		this.value = value;
		this.timestamp = timestamp.toDateTime(DateTimeZone.UTC);
		this.source = source;
	}

	public CurrencyPair getCurrencyPair() {
		return currencyPair;
	}

	public BigDecimal getValue() {
		return value;
	}

	public DateTime getTimestamp() {
		return timestamp;
	}

	@JsonIgnore
	public String getFormattedTimestamp() {
		return timestamp.toString(TIMESTAMP_PATTERN);
	}

	public RateSource getSource() {
		return source;
	}

	public String getTitle() {
		return String.format("%s -> %s", currencyPair.getFrom().title(), currencyPair.getTo().title());
	}

	@Override
	public int hashCode() {
		return Objects.hash(currencyPair, value, timestamp, source);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExchangeRate other = (ExchangeRate) obj;
		return Objects.equals(currencyPair, other.currencyPair) && Objects.equals(value, other.value)
				&& Objects.equals(timestamp, other.timestamp) && Objects.equals(source, other.source);
	}

	@Override
	public String toString() {
		return String.format("%s : %.10f on %s by %s", currencyPair, value, timestamp.toString(TIMESTAMP_PATTERN),
				source);
	}

	@Override
	public int compareTo(ExchangeRate other) {
		return currencyPair.compareTo(other.currencyPair);
	}
}
