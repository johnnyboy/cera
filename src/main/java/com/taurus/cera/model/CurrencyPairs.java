package com.taurus.cera.model;

import java.util.Collection;
import java.util.HashSet;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class CurrencyPairs {
	private CurrencyPairs() {
	}

	private static class CastAwayPredicate implements Predicate<CurrencyPair> {

		private final CurrencyCode castAway;

		public CastAwayPredicate(CurrencyCode castAway) {
			this.castAway = castAway;
		}

		@Override
		public boolean apply(CurrencyPair input) {
			return !input.contains(castAway);
		}
	}

	public static Collection<CurrencyPair> all() {
		CurrencyCode[] codes = CurrencyCode.values();
		Collection<CurrencyPair> allPairs = new HashSet<>(codes.length * codes.length - 1);
		for (CurrencyCode code : codes) {
			allPairs.addAll(code.pairs());
		}
		return allPairs;
	}

	public static Collection<CurrencyPair> allBut(CurrencyCode castAway) {
		return Lists.newArrayList(Iterables.filter(all(), new CastAwayPredicate(castAway)));
	}

}
