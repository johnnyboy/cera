package com.taurus.cera.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Function;

public class StatisticalExchangeRate {

	private final CurrencyPair pair;

	private final Collection<KafkaExchangeRate> kafkaRates;

	private BigDecimal minimalRate;

	private BigDecimal averageRate;

	private BigDecimal maximumRate;

	@JsonCreator
	public StatisticalExchangeRate(@JsonProperty("pair") CurrencyPair pair,
			@JsonProperty("kafkaRates") Collection<KafkaExchangeRate> kafkaRates) {
		checkNotNull(pair);
		checkNotNull(kafkaRates);
		this.pair = pair;
		this.kafkaRates = kafkaRates;
	}

	private BigDecimal calculateMinimal() {
		return (kafkaRates.isEmpty()) ? BigDecimal.ZERO : Collections.min(rates());
	}

	private BigDecimal calculateAverage() {
		if (kafkaRates.isEmpty()) {
			return BigDecimal.ZERO;
		}
		BigDecimal total = BigDecimal.ZERO;
		for (KafkaExchangeRate kafkaRate : kafkaRates) {
			total = total.add(kafkaRate.getRate().getValue());
		}
		return total.divide(BigDecimal.valueOf(kafkaRates.size()), RoundingMode.HALF_UP);
	}

	private BigDecimal calculateMaximum() {
		return (kafkaRates.isEmpty()) ? BigDecimal.ZERO : Collections.max(rates());
	}

	private Collection<BigDecimal> rates() {
		Collection<BigDecimal> rates = newArrayList(transform(kafkaRates, new RateCollector()));
		return rates;
	}

	public BigDecimal getMinimalRate() {
		return (minimalRate == null) ? calculateMinimal() : minimalRate;
	}

	public BigDecimal getRoundedMinimalRate() {
		return scale(getMinimalRate());
	}

	public BigDecimal getAverageRate() {
		return (averageRate == null) ? calculateAverage() : averageRate;
	}

	public BigDecimal getRoundedAverageRate() {
		return scale(getAverageRate());
	}

	public BigDecimal getMaximumRate() {
		return (maximumRate == null) ? calculateMaximum() : maximumRate;
	}

	public BigDecimal getRoundedMaximumRate() {
		return scale(getMaximumRate());
	}

	private BigDecimal scale(BigDecimal value) {
		return value.setScale(3, RoundingMode.HALF_UP);
	}

	public CurrencyPair getPair() {
		return pair;
	}

	public Collection<KafkaExchangeRate> getKafkaRates() {
		return kafkaRates;
	}

	@Override
	public int hashCode() {
		return Objects.hash(pair, kafkaRates);
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (!this.getClass().equals(obj.getClass())) {
			return false;
		}
		StatisticalExchangeRate other = (StatisticalExchangeRate) obj;
		return Objects.equals(pair, other.pair) && Objects.equals(kafkaRates, other.kafkaRates);
	}

	@Override
	public String toString() {
		return String.format("AverageExchangeRate pair:%s rate:%.10f per %d elements", pair, averageRate,
				kafkaRates.size());
	}

	private static class RateCollector implements Function<KafkaExchangeRate, BigDecimal> {

		@Override
		public BigDecimal apply(KafkaExchangeRate input) {
			return input.getRate().getValue();
		}
	}
}
