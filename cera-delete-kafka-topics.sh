#!/bin/bash
KAFKA_BIN_PATH="/home/tbyndyu/kafka/kafka_2.11-0.10.1.0/bin"

echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic CNY_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic CNY_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic CNY_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic CNY_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic CNY_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic EUR_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic EUR_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic EUR_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic EUR_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic EUR_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic GBP_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic GBP_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic GBP_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic GBP_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic GBP_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic PLN_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic PLN_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic PLN_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic PLN_UAH`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic PLN_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic UAH_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic UAH_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic UAH_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic UAH_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic UAH_USD`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic USD_CNY`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic USD_EUR`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic USD_GBP`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic USD_PLN`
echo `$KAFKA_BIN_PATH/kafka-topics.sh --zookeeper localhost:2181 --delete --topic USD_UAH`